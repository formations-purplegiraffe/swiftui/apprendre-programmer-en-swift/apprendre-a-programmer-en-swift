//
//  main.swift
//  AppConsole
//
//  Created by Maxime Britto on 02/05/2017.
//  Copyright © 2017 mbritto. All rights reserved.
//

import Foundation

func optionnelsIfLet() {
    var age:Int
    print("Veuillez saisir votre âge :")
    let ageTexte:String = Utilisateur.saisirTexte()
    if let ageSaisi = Int(ageTexte) {
        age = ageSaisi
    } else {
        print("Veuillez saisir un entier valide.")
    }
}

func optionnelsValeurParDefaut() {
    var age : Int
    print("Veuillez saisir votre âge :")
    let ageTexte:String = Utilisateur.saisirTexte()
    age = Int(ageTexte) ?? 0
}

//Mode qui désactive la sécurité du compilateur pour la nullité (point exclamation)
func optionnelsEnModeKamikase() {
    var age : Int
    print("Veuillez saisir votre âge :")
    let ageTexte:String = Utilisateur.saisirTexte()
    age = Int(ageTexte)!
}
optionnelsIfLet()
optionnelsValeurParDefaut()





//
//  main.swift
//  AppConsole
//
//  Created by Maxime Britto on 02/05/2017.
//  Copyright © 2017 mbritto. All rights reserved.
//

import Foundation


func menu() {
    
    let nomUtilisateur = demanderNomUtilisateur()
    if (nomUtilisateur.count < 3) {
        print("Votre nom est trop court (< 3 lettres)")
        menu()
    } else if (nomUtilisateur.count > 20) {
        print("Votre nom est trop long (> 20 lettres)")
        menu()
    }
    
    var choixUtilisateur:Int
    repeat {
        repeat {
            print("1 - Démarrer une partie")
            print("2 - Accéder aux réglages")
            print("3 - Quitter le jeu")
            print("Entrez votre votre choix : ")
            choixUtilisateur = Utilisateur.saisirEntier()
        } while choixUtilisateur < 1 || choixUtilisateur > 3


        switch choixUtilisateur {
        case 1:
            demarrerPartie(nom: nomUtilisateur)
        case 2:
            afficherPreferences(nom: nomUtilisateur)
        case 3:
            quitterApplication(nom: nomUtilisateur)
        default:
            break
        }
    } while choixUtilisateur != 3
}

menu()

func demarrerPartie(nom:String){
    print(nom + " débute la partie...")
}

func afficherPreferences(nom:String){
    print(nom + " accède aux préférences")
}

func quitterApplication(nom:String){
    print("Bye bye \(nom)")
    
}

func demanderNomUtilisateur() -> String{
    print("Bonjour, comment vous appelez-vous ?")
    let nomUtilisateur = Utilisateur.saisirTexte()
    return nomUtilisateur
}






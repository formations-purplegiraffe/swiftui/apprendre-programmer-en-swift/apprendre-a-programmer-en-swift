import Foundation

func fonctionPrincipale()
{
    tableauxNumerotes()
    tableauClesValeurs()
}

fonctionPrincipale()

func tableauxNumerotes()
{
    let ennemisMario:[String] = ["Bowser", "Wario", "Koopa", "Boo"]
    print(ennemisMario[0])
    
    var amisMario:[String] = ["Luigi"]
    amisMario.append("Peach")
    amisMario.append("Toad")
    print(amisMario[0])
    print(amisMario[1])
    amisMario.insert("Yoshi", at: 1)
    print(amisMario[1])
    print(amisMario[2])
    amisMario.remove(at: 0)
    print(amisMario[amisMario.count-1])
    
    print("Liste des ennemis de Mario : ")
    for ennemi in ennemisMario {
        print(" - \(ennemi)")
    }
    
    /* .enumerated() permet de récupérer le numéro de la case dans le tableau */
    for (numeroCase, ennemi) in ennemisMario.enumerated() {
        print(" - \(ennemi) : \(numeroCase)")
    }
}

func tableauClesValeurs()
{
    var scoreJoueurs:[String:Int] = ["Mario":10, "Luigi":9]
    print(scoreJoueurs["Mario"])
    scoreJoueurs["Toad"] = 12
    scoreJoueurs.removeValue(forKey: "Luigi")
    
    print("Scores de tous les joueurs")
    for (nomJoueur, score) in scoreJoueurs {
        print(" - \(nomJoueur) : \(score)")
    }
    
}
